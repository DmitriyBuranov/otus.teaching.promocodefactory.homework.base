﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> 
        : IRepository<T> 
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }


        public T Create(T entity)
        {
            var newCollection = Data.ToList();
            newCollection.Add(entity);
            Data = newCollection;

            return entity;
        }


        public async Task UpdateAsync(T entity)
        {
            var newCollection = Data.ToList();
            var removableObject = await GetByIdAsync(entity.Id);
            newCollection.Remove(removableObject);
            newCollection.Add(entity);
          
        }

        public void Remove(T entity)
        {
            var newCollection = Data.ToList();
            newCollection.Remove(entity);
            Data = newCollection;
        }



    }
}