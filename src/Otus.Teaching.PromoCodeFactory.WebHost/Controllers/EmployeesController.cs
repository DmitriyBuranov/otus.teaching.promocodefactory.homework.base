﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        private readonly IRepository<Role> _repositoryRoles;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> repositoryRoles)
        {
            _employeeRepository = employeeRepository;
            _repositoryRoles = repositoryRoles;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }


        /// <summary>
        /// Creates Employee
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreatetEmployeeAsync(EmployeeDto request)
        {
            var entity = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                Email = request.Email,
                LastName = request.LastName,
                AppliedPromocodesCount = request.AppliedPromocodesCount
            };
            var roles = await GetRoles(request);

            entity.Roles= roles;

            if(roles.Count == 0)
                return NotFound();

            _employeeRepository.Create(entity);

            return Ok(entity.Id);

        }

        /// <summary>
        /// Updates Employee
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <param name="request">Employee Dto</param> 
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatetEmployeeAsync(Guid id, EmployeeDto request)
        {
            var entity = await _employeeRepository.GetByIdAsync(id);

            if (entity == null)
                return NotFound();

            entity.FirstName = request.FirstName;
            entity.Email = request.Email;
            entity.LastName = request.LastName;
            entity.AppliedPromocodesCount = request.AppliedPromocodesCount;

            var roles = await GetRoles(request);
            entity.Roles = roles;

            await _employeeRepository.UpdateAsync(entity);

            return Ok();
        }

        /// <summary>
        /// Deletes Employee
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeletetEmployee(Guid id)
        {
            var entity = await _employeeRepository.GetByIdAsync(id);

            if (entity == null)
                return NotFound();

            _employeeRepository.Remove(entity);

            return Ok();
        }

        /// <summary>
        /// Gets or Creates Role from Employee Dto
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private async Task<List<Role>> GetRoles(EmployeeDto request)
        {
            var idRoles = request.IdRoles;

            List<Role> roles = new List<Role>();

            foreach (Guid id in idRoles)
            {
                var role = await _repositoryRoles.GetByIdAsync(id);
                if (role == null)
                {
                    //ошибка?
                }
                else
                {
                    roles.Add(role);
                }
            }

            return  roles;
        }
    }
}